package test;

import mint.validate.Valid;

public class User extends People{
	/*采用正则表达式验证（可配置多个正则表达式）*/
	@Valid(tipMsg="邮箱地址不正确", pattern={"^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$"})
	public String email;
	
	/*简单验证器验证*/
	@Valid(rule="notnull")
	public String username;
	
	/*采用验证器验证，并给验证器传递参数*/
	@Valid(rule={"LenLimit"}, params={"6", "32"})
	public String password;
}
