package test;

import mint.validate.Valid;
import mint.validate.Verifiable;

public class People extends Verifiable{
	public int gender;
	
	@Valid(rule="notnull")
	public String address;
}
