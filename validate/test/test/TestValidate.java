package test;

import mint.validate.VerificationResult;

import org.junit.Test;

public class TestValidate {

	@Test
	public void test() {
		User u = new User();
		
		u.email = "895925636#qq.com";
		u.password = "passw";
		u.address = "中国广东";

		/*不写字段名*/
		u.validate();
		
		for(VerificationResult r : u.getValidateResult()){
			System.out.println(r);
		}
	}
}