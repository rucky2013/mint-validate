package verifiers;

import mint.validate.VerificationResult;
import mint.validate.Verifier;

public class NotNullVerifier implements Verifier{

	public VerificationResult verify(Object bean, Class<?> fieldType, Object fieldValue, String fieldName, String[] params) {
		if(fieldValue == null || "".equals(fieldValue)){
			return new VerificationResult(fieldName, "NotNull", fieldName+"不能为空", fieldValue, false);
		}
		
		return null;
	}

}
