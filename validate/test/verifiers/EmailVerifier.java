package verifiers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mint.validate.VerificationResult;
import mint.validate.Verifier;

public class EmailVerifier implements Verifier{

	@Override
	public VerificationResult verify(Object bean, Class<?> fieldType, Object fieldValue, String fieldName, String[] params) {
		boolean result = false;
		if(fieldValue != null){
			Pattern pattern = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$");
			Matcher matcher = pattern.matcher((String)fieldValue);
			result = matcher.matches();
		}
		
		if(!result){
			return new VerificationResult(fieldName, "email", "邮箱格式不对", fieldValue, result);
		}
		
		return null;
	}

}
