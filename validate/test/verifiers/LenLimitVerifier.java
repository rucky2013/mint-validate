package verifiers;

import mint.validate.ValidateException;
import mint.validate.VerificationResult;
import mint.validate.Verifier;

public class LenLimitVerifier implements Verifier{

	@Override
	public VerificationResult verify(Object bean, Class<?> fieldType, Object fieldValue, String fieldName, String[] params) {
		if(fieldValue == null) return new VerificationResult(fieldName, "lenlimit", "字段值为空",	 null, false);
		
		if(params.length < 2){
			throw new ValidateException("没有指定合适参数");
		}
		
		int min, max;
		try{
			min = Integer.parseInt(params[0]); 
			max = Integer.parseInt(params[1]);
		} catch (NumberFormatException e){
			e.printStackTrace();
			throw new ValidateException("参数格式错误，无法转换成数字");
		}
		
		int len = ((String)fieldValue).length();
		if(len < min){
			return new VerificationResult(fieldName, "lenlimit", fieldName+"不能少于"+min+"个字符", fieldValue, false);
		}
		
		if(len > max){
			return new VerificationResult(fieldName, "lenlimit", fieldName+"不能多于"+max+"个字符", fieldValue, false);
		}
		
		return null;
	}

}
