package mint.validate;


public interface Verifier {
	public VerificationResult verify(Object bean, Class<?> fieldType, Object fieldValue, String fieldName, String[] params);
}
